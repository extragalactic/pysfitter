from setuptools import setup
import os
import sys
import pysfitter

_here = os.path.abspath(os.path.dirname(__file__))

if sys.version_info[0] < 3:
    with open(os.path.join(_here, 'README.md')) as f:
        long_description = f.read()
else:
    with open(os.path.join(_here, 'README.md'), encoding='utf-8') as f:
        long_description = f.read()

setup(
    name='pysfitter',
    version=pysfitter.__version__,
    description=('Package for galaxy and stellar spectra fitting.'),
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='Ivan Katkov',
    author_email='katkov.ivan@gmail.com',
    url='https://github.com/ivan-katkov/pysfitter',
    license='MIT',
    packages=['pysfitter'],
#   no dependencies in this example
#   install_requires=[
#       'dependency==1.2.3',
#   ],
#   no scripts in this example
#   scripts=['bin/a-script'],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Astronomy',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'],
    )
