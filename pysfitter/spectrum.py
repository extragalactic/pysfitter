from __future__ import print_function, division

import numpy as np
from scipy import interpolate
from astropy.io import fits
from pysfitter import utils


class Spectrum(object):

    def __init__(self, file, ext=0, err_file=None, err_ext=0, velscale=40.0,
                 norm=None):
        """
        A Spectrum class that can be used to read spectrum from the fits file,
        prepare data including rescaling to given wavelength range, resolution,
        spatial rebinning in case of long-slit or IFU data etc.

        Reading and logrebinning is realized right now.

        Parameters
        ----------
        file : str
            File with spectrum.
        ext : int or str
            Extension number or name where spectrum is stored.
        err_file : str
            File with error spectrum.
        err_ext : int or str
            Extension number or name with error spectrum.
        velscale : float
            Size of logrebinned spectrum in terms of km/s.
        norm : str or float
            Specify normalization factor or 'median' that means that median value
            will be used.
        """

        self.file = file
        self.file_ext = ext
        self.err_file = err_file
        self.err_ext = err_ext
        self.hdr = None
        self.waves = None
        self.wave_range = None
        self.flux = None
        self.error = None
        self.logrebinned = None
        self.velscale = velscale
        self.dim = None
        self.normfactor = norm
        self.binnum = None

        # read the file
        self._read()

        # logrebin spectrum and error spectrum
        self.flux, log_waves = self.logrebin(self.flux, self.waves, self.velscale)
        if self.error is not None:
            self.error, _ = self.logrebin(self.error, self.waves, self.velscale)
        self.waves = log_waves
        self.logrebinned = True

        # normalized spectrum
        if self.normfactor is None:
            self.normfactor = np.nanmedian(self.flux)
        self.flux /= self.normfactor
        if self.error is not None:
            self.error /= self.normfactor

    def _read(self):
        """
        This method reads the spectrum from fits file and filled some attributes
        of the Spectrum object.
        """
        with fits.open(self.file) as hdu:
            self.flux = hdu[self.file_ext].data
            self.hdr = hdu[self.file_ext].header
            self.wave_range, self.waves = utils.get_waves(self.hdr)
            self.logrebined = False
            self.dim = len(self.flux.shape)

        # read error frame
        if self.err_file is not None:
            with fits.open(self.err_file) as hdu_error:
                self.error = hdu_error[self.err_ext].data

    @staticmethod
    def logrebin(spec, waves, velscale, kind='cubic', outwaves=True):
        """
        This function to rebin input spectrum to logarithmic wavelengths for
        given velscale parameter.

        :param array_like spec:  Input spectrum to be logrebinned.
        :param array_like waves: Input vector of wavelengths.
        :param float velscale: Size of pixel in logrebinned spectrum in terms
            of km/s.
        :param str kind: Type of inteprolation. Default is cubic. For details
            see :class:`scipy.interpolate.interp1d`.
        :param boolean outwaves: If `True` array with wavelengths is returned
            in addition to the logrebinned spectrum is returned. Default:
            ``True``.
        :return: Logrebbined spectrum or tuple like (`log_spec`, `log_waves`)
            in case of ``outwaves = True``.
        """

        log_waves = np.exp(
            np.arange(np.log(np.min(waves)), np.log(np.max(waves)),
                      velscale / 299792.45))
        fnc = interpolate.interp1d(waves, spec, kind=kind,
                                   fill_value='extrapolate')
        log_spec = fnc(log_waves)

        if outwaves:
            return log_spec, log_waves
        else:
            return log_spec

    def __binning_prepare(self, reference_waves=[5000, 10], wave_axis=1):

        # mask and extract spectral region where SN estimated
        mask = np.abs(self.waves - reference_waves[0]) < reference_waves[1]

        sub_flux = np.take(self.flux, np.flatnonzero(mask), axis=wave_axis)
        sub_err = np.take(self.error, np.flatnonzero(mask), axis=wave_axis)

        # claculate SN for inbinned pixels
        signal = np.nanmedian(sub_flux, axis=wave_axis)
        noise = np.nanmedian(sub_err, axis=wave_axis)
        self.unbinned_signal = signal
        self.unbinned_noise = noise

    def __bin_pars(self):
        """Calculate bin parameters (SN, xpos, binned specta etc.) based on
        binning schema `binnum`."""

        nbins = np.max(self.binnum) + 1
        bin_pos = np.zeros(nbins, dtype=float)
        bin_sn = np.zeros(nbins, dtype=float)
        bin_size = np.zeros(nbins, dtype=int)
        flux_binned = np.zeros((nbins, self.waves.size), dtype=float)
        error_binned = np.zeros((nbins, self.waves.size), dtype=float)
        xx = np.arange(self.binnum.size)
        for ibin in range(nbins):
            msk = (self.binnum == ibin)
            # SN in the given bin
            bin_sn[ibin] = np.sum(self.unbinned_signal[msk])/np.sqrt(np.sum(self.unbinned_noise[msk]**2))
            # luminosity weighted position of the given bin
            bin_pos[ibin] = np.sum(xx[msk]*self.unbinned_signal[msk])/np.sum(self.unbinned_signal[msk])
            bin_size[ibin] = len(msk)
            flux_binned[ibin, :] = np.sum(self.flux[msk, :], axis=0)
            error_binned[ibin, :] = np.sqrt(np.sum(self.error[msk, :]**2, axis=0))

        self.bin_sn = bin_sn
        self.bin_pos = bin_pos
        self.bin_size = bin_size
        self.flux_binned = flux_binned
        self.error_binned = error_binned

    def make_binning_adaptive(self, snr=30.0, snr_min=3.0, binsize_min=1,
                              reference_waves=[5000, 10], wave_axis=1):
        """Make adaptive binning of the given spectrum.

        .. warning::
            Currently the method developed only for long-slit spectrum.

        Parameters
        ----------
        snr : float
            Required signal-to-noise (SN) ratio per bin.
        snr_min : float
            Minimal SN of unbunned pixel which will be used for binning process.
        binsize_min : int
            Minimal size of bins.
        reference_waves : 2-tuple of floats
            Small spectral range to calculate SN of the unbinned spectrum.
            First element is reference wavelength; second -- window around
            reference wavelength.It should be used out of strong absorption
            lines, emission or night sky lines.
        wave_axis : int
            Index of wavelength axis along which
        """

        self.__binning_prepare(reference_waves=reference_waves, wave_axis=wave_axis)

        s_n = self.ubninned_signal / self.ubninned_noise

        # make binning
        binnum = np.zeros(signal.size, dtype=int) - 1
        mask_minsnr = np.flatnonzero((s_n >= snr_min))
        imax = np.argmax(s_n[mask_minsnr])
        cur_bin = 0
        cur_binsize = 0
        cur_signal = 0
        cur_noise = 0
        for i in range(imax, mask_minsnr.size):
            cur_signal += signal[mask_minsnr[i]]
            cur_noise += noise[mask_minsnr[i]]**2
            cur_binsn = cur_signal/np.sqrt(cur_noise)
            cur_binsize += 1
            binnum[mask_minsnr[i]] = cur_bin
            if cur_binsn >= snr and cur_binsize >= binsize_min:
                cur_signal = 0.0
                cur_noise = 0.0
                cur_binsize = 0
                cur_bin += 1
        cur_signal = 0.0
        cur_noise = 0.0
        cur_binsize = 0
        for i in range(imax-1, -1, -1):
            cur_signal += signal[mask_minsnr[i]]
            cur_noise += noise[mask_minsnr[i]]**2
            cur_binsn = cur_signal/np.sqrt(cur_noise)
            cur_binsize += 1
            binnum[mask_minsnr[i]] = cur_bin
            if cur_binsn >= snr and cur_binsize >= binsize_min:
                cur_signal = 0.0
                cur_noise = 0.0
                cur_binsize = 0
                cur_bin += 1

        self.binnum = binnum

        # calculate additial values like SN, xpos for given bin
        self.__bin_pars()

    def make_binning_userdef(self, regions, reference_waves=[5000, 10],
                             wave_axis=1):
        """Make a user defined binning.

        Parameters
        ----------
        regions : sequence of 2-elements
            Each element of sequence specify limits of each spatial bin.
        """
        self.__binning_prepare(reference_waves=reference_waves, wave_axis=wave_axis)

        binnum = np.zeros(self.unbinned_signal.size, dtype=int) - 1
        for idx, region in enumerate(regions):
            binnum[region[0]:region[1]] = idx

        self.binnum = binnum

        # calculate additial values like SN, xpos for given bin
        self.__bin_pars()
