"""
This module provides a LSF (Line Spread Function) object which widely used
for the spectral fitting.
"""

from __future__ import print_function, division

import numpy as np
import lmfit
from matplotlib import pyplot as plt
from pysfitter import utils
from astropy.io import fits
from astropy import convolution
from scipy import signal, fftpack, optimize

from pysfitter import utils

# import matplotlib
# matplotlib.use('TkAgg')

class LSF(object):
    pass
