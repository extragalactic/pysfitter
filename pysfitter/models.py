"""
Objects for dealing with different kind of fitting models.
"""

from __future__ import print_function, division

import numpy as np
import lmfit
from matplotlib import pyplot as plt
from astropy import convolution

from pysfitter import utils

class OneSSP(object):

    def __init__(self, interp):
        """
        Class to make modelling of the spectra by model consisting 1 SSP component.

        Parameters
        ----------
        interpolator : :class:`.InterpolatorMiles`
            Instance of the Interpolator which will be used to calculate stellar
            population template.
        """
        self.interp = interp

    def _residuals(self, p, flux=None, error=None):

        model_spec = self.interp.eval(p['lage'], p['met'], p['mgfe'])

        model_spec_conv = utils.conv_gaus(model_spec,
                                          p['vel']/self.interp.velscale,
                                          p['sig']/self.interp.velscale)

        xx = np.linspace(-1, 1, flux.size)
        coefs = [p[pr].value for pr in p if 'mpoly' in pr]
        mpoly = np.polynomial.legendre.legval(xx, coefs)

        self.flux = flux
        self.error = error
        self.model = mpoly * model_spec_conv[self.interp.waves_msk]
        self.mpoly = mpoly

        return (flux - mpoly * model_spec_conv[self.interp.waves_msk])/error

    def __print_output(self, p, chi2=-1, iter=-1):
        pr = (p['vel'].value, p['sig'].value, p['age'].value,
              p['met'].value, p['mgfe'].value)

        print("Iter {:>6d}; v={:7.2f}; s={:7.2f}; T={:>5.2f}, Z={:5.2f}; A={:5.2f}; redchi={:f}".format(
            iter, pr[0], pr[1], pr[2], pr[3], pr[4], chi2))

    def __print_output_bin(self, sol):
        p = sol.params
        pr = (p['vel'].value, p['sig'].value, p['age'].value,
              p['met'].value, p['mgfe'].value)
        epr = (p['vel'].stderr, p['sig'].stderr, p['age'].stderr,
              p['met'].stderr, p['mgfe'].stderr)

        print("N {:>6d}; v={:7.2f}; s={:7.2f}; T={:>5.2f}, Z={:5.2f}; A={:5.2f}; redchi={:f}".format(
            sol.nfev, pr[0], pr[1], pr[2], pr[3], pr[4], sol.redchi))
        if sol.errorbars:
            print("            {:7.2f};   {:7.2f};   {:>5.2f},   {:5.2f};   {:5.2f}".format(
                epr[0], epr[1], epr[2], epr[3], epr[4]))

    def __plot(self):
        plt.cla()
        plt.plot(self.spectrum.waves, self.flux)
        plt.plot(self.spectrum.waves, self.model, color='red')
        plt.plot(self.spectrum.waves, self.flux-self.model, 'k+', ms=3)
        plt.plot(self.spectrum.waves,
                 self.mpoly/np.median(self.mpoly)*np.nanmedian(self.flux), lw=0.5)
        plt.plot(self.spectrum.waves, self.error, color='grey', alpha=0.7)
        plt.plot(self.spectrum.waves, -self.error, color='grey', alpha=0.7)
        plt.pause(0.0001)

    def _residuals_iter_cb(self, p, iter, resid, flux=None, error=None):
        if self.verbose is 'iter':
            self.__print_output(p, chi2=np.sum(resid**2), iter=iter)
        if self.plot is 'iter':
            self.__plot()

    def fit(self, spectrum, guess_kin=[1000.0, 100.0], guess_stpop=[4.0, -0.1, 0.0],
            vsys=5000.0, vary_kin=[True, True], vary_stpop=[True, True, True],
            mdegree=12, vamp=600.0, samp=400.0, method='leastsq', plot='bin',
            verbose='bin'):
        """
        Fitting spectrum by stellar population model computed based on
        given inteprolator.

        Parameters
        ----------
        spectrum : :class:`.Spectrum`
            Spectrum object to be fitted.

        Returns
        -------
        fit_results : :class:`lmfit.minimize`
            Fitting results.
        """
        self.spectrum = spectrum
        self.verbose = verbose
        self.plot = plot

        # Prepare minimization
        pars = lmfit.Parameters()
        lages = np.log10(self.interp.ages)
        pars.add('lage', value=np.log10(guess_stpop[0]), min=np.min(lages),
                 max=np.max(lages), vary=vary_stpop[0])
        pars.add('age', expr='10**lage')
        pars.add('met', value=guess_stpop[1], min=np.min(self.interp.mets),
                 max=np.max(self.interp.mets), vary=vary_stpop[1])
        pars.add('mgfe', value=guess_stpop[2], min=np.min(self.interp.mgfes),
                 max=np.max(self.interp.mgfes), vary=vary_stpop[2])
        pars.add('vel', value=guess_kin[0], min=-vamp, max=vamp, vary=vary_kin[0])
        pars.add('sig', value=guess_kin[1], min=self.spectrum.velscale, max=samp,
                 vary=vary_kin[0])
        pars.add('mpoly0', value=1.0)
        for k in range(mdegree):
            pars.add('mpoly{}'.format(k+1), value=0.0)


        if spectrum.binnum is None:
            raise TypeError("Spectrum object is not binned properly.")
        else:
            nbins = np.max(spectrum.binnum)+1
            solutions = []
            for ibin in range(nbins):
                kws = dict(flux=spectrum.flux_binned[ibin, :],
                           error=spectrum.error_binned[ibin, :])

                if method is 'leastsq':
                    fit_kws = dict(epsfcn=0.03, ftol=1e-4)
                else:
                    fit_kws = dict()

                # update initial values for mpoly  for given bin
                model0 = self.interp.eval(pars['lage'], pars['met'], pars['mgfe'])
                model0 = model0[self.interp.waves_msk]

                mpoly_guess = np.polynomial.Legendre.fit(np.arange(model0.size),
                                                  kws['flux']/model0, mdegree)
                for k in range(mdegree+1):
                    mpoly_par = "mpoly{}".format(k)
                    pars[mpoly_par].value = mpoly_guess.coef[k]
                    pars[mpoly_par].min = mpoly_guess.coef[k]/2
                    pars[mpoly_par].max = 2*mpoly_guess.coef[k]



                sol = lmfit.minimize(self._residuals, pars, nan_policy='omit',
                                     iter_cb=self._residuals_iter_cb,
                                     kws=kws, method=method, **fit_kws)
                sol = lmfit.minimize(self._residuals, sol.params, nan_policy='omit',
                                     iter_cb=self._residuals_iter_cb,
                                     kws=kws, method='leastsq', **fit_kws)
                solutions.append(sol)
                if sol.success:
                    self.__print_output_bin(sol)
                    self.__plot()
                else:
                    raise ValueError("Bad fitting iterationt.")

        return solutions


class OneSSP999(object):

    def __init__(self, interp, losvd):
        """
        Class to make modelling of the spectra by model consisting 1 SSP component.

        Parameters
        ----------
        interpolator : :class:`.InterpolatorMiles`
            Instance of the Interpolator which will be used to calculate stellar
            population template.
        """
        self.interp = interp
        self.losvd = losvd

    def _residuals(self, p, flux=None, error=None):
        # print(p)
        self.losvd.set_params(p)
        kernel = self.losvd.get_kernel(velscale=self.interp.velscale)
        model_spec = self.interp.eval(p['lage'], p['met'], p['mgfe'])

        model_spec_conv = convolution.convolve(model_spec, kernel)#, normalize_kernel=False)

        xx = np.linspace(-1, 1, flux.size)
        coefs = [p[pr].value for pr in p if 'mpoly' in pr]
        mpoly = np.polynomial.legendre.legval(xx, coefs)

        self.flux = flux
        self.error = error
        self.model = mpoly * model_spec_conv[self.interp.waves_msk]
        self.mpoly = mpoly

        # return (flux - mpoly * model_spec_conv[self.interp.waves_msk])/error
        return (self.flux - self.model) / self.error

    def __print_output(self, p, chi2=-1, iter=-1):
        pr = (p['vel'].value, p['sig'].value, p['age'].value,
              p['met'].value, p['mgfe'].value)

        print("Iter {:>6d}; v={:7.2f}; s={:7.2f}; T={:>5.2f}, Z={:5.2f}; A={:5.2f}; redchi={:f}".format(
            iter, pr[0], pr[1], pr[2], pr[3], pr[4], chi2))
        print("h3: {:9.3f}; h4: {:9.3f}".format(p['h3'].value, p['h4'].value))

    def __print_output_bin(self, sol):
        p = sol.params
        pr = (p['vel'].value, p['sig'].value, p['age'].value,
              p['met'].value, p['mgfe'].value)
        epr = (p['vel'].stderr, p['sig'].stderr, p['age'].stderr,
              p['met'].stderr, p['mgfe'].stderr)

        print("N {:>6d}; v={:7.2f}; s={:7.2f}; T={:>5.2f}, Z={:5.2f}; A={:5.2f}; chi={:f}; redchi={:f}".format(
            sol.nfev, pr[0], pr[1], pr[2], pr[3], pr[4], sol.chisqr, sol.redchi))
        print("h3: {:9.3f}; h4: {:9.3f}".format(p['h3'].value, p['h4'].value))
        if sol.errorbars:
            print("            {:7.2f};   {:7.2f};   {:>5.2f},   {:5.2f};   {:5.2f}".format(
                epr[0], epr[1], epr[2], epr[3], epr[4]))
            print("    {:9.3f};     {:9.3f}".format(p['h3'].stderr, p['h4'].stderr))

    def __plot(self):
        plt.cla()
        plt.plot(self.spectrum.waves, self.flux)
        plt.plot(self.spectrum.waves, self.model, color='red')
        plt.plot(self.spectrum.waves, self.flux-self.model, 'k+', ms=3)
        plt.plot(self.spectrum.waves,
                 self.mpoly/np.median(self.mpoly)*np.nanmedian(self.flux), lw=0.5)
        plt.plot(self.spectrum.waves, self.error, color='grey', alpha=0.7)
        plt.plot(self.spectrum.waves, -self.error, color='grey', alpha=0.7)
        plt.pause(0.0001)

    def _residuals_iter_cb(self, p, iter, resid, flux=None, error=None):
        if self.verbose is 'iter':
            self.__print_output(p, chi2=np.sum(resid**2), iter=iter)
        if self.plot is 'iter':
            self.__plot()

    def fit(self, spectrum, guess_kin=[1000.0, 100.0], guess_stpop=[4.0, -0.1, 0.0],
            vsys=5000.0, vary_kin=[True, True], vary_stpop=[True, True, True],
            mdegree=12, vamp=600.0, samp=400.0, method='leastsq', plot='bin',
            verbose='bin'):
        """
        Fitting spectrum by stellar population model computed based on
        given inteprolator.

        Parameters
        ----------
        spectrum : :class:`.Spectrum`
            Spectrum object to be fitted.

        Returns
        -------
        fit_results : :class:`lmfit.minimize`
            Fitting results.
        """
        self.spectrum = spectrum
        self.verbose = verbose
        self.plot = plot

        # Prepare minimization
        pars = lmfit.Parameters()
        lages = np.log10(self.interp.ages)
        pars.add('lage', value=np.log10(guess_stpop[0]), min=np.min(lages),
                 max=np.max(lages), vary=vary_stpop[0])
        pars.add('age', expr='10**lage')
        pars.add('met', value=guess_stpop[1], min=np.min(self.interp.mets),
                 max=np.max(self.interp.mets), vary=vary_stpop[1])
        pars.add('mgfe', value=guess_stpop[2], min=np.min(self.interp.mgfes),
                 max=np.max(self.interp.mgfes), vary=vary_stpop[2])
        losvd_parameters = self.losvd.get_params()
        pars += losvd_parameters
        # pars.add('vel', value=guess_kin[0], min=-vamp, max=vamp, vary=vary_kin[0])
        # pars.add('sig', value=guess_kin[1], min=self.spectrum.velscale, max=samp,
        #          vary=vary_kin[0])
        pars.add('mpoly0', value=1.0)
        for k in range(mdegree):
            pars.add('mpoly{}'.format(k+1), value=0.0)


        if spectrum.binnum is None:
            raise TypeError("Spectrum object is not binned properly.")
        else:
            nbins = np.max(spectrum.binnum)+1
            solutions = []
            for ibin in range(nbins):
                kws = dict(flux=spectrum.flux_binned[ibin, :],
                           error=spectrum.error_binned[ibin, :])

                # update initial values for mpoly  for given bin
                model0 = self.interp.eval(pars['lage'], pars['met'], pars['mgfe'])
                model0 = model0[self.interp.waves_msk]

                mpoly_guess = np.polynomial.Legendre.fit(np.arange(model0.size),
                                                  kws['flux']/model0, mdegree)
                for k in range(mdegree+1):
                    mpoly_par = "mpoly{}".format(k)
                    pars[mpoly_par].value = mpoly_guess.coef[k]
                    # pars[mpoly_par].min = mpoly_guess.coef[k]/2.0
                    # pars[mpoly_par].max = 2.0*mpoly_guess.coef[k]


                for metod in list(method):
                    print("method: {}".format(metod) )

                    if metod is 'leastsq':
                        fit_kws = dict(epsfcn=0.03, ftol=1e-4)
                    else:
                        fit_kws = dict()

                    sol = lmfit.minimize(self._residuals, pars, nan_policy='omit',
                                         iter_cb=self._residuals_iter_cb,
                                         kws=kws, method=metod, **fit_kws)
                    pars = sol.params

                solutions.append(sol)
                if sol.success:
                    self.__print_output_bin(sol)
                    self.__plot()
                else:
                    raise ValueError("Bad fitting iteration.")

        return solutions



class TwoSSP(object):
    pass


class ExpSFH(object):

    pass


class ComplexSFH(object):
    pass


class Fitter(object):

    def __init__(self, template=None, losvd=None):
        """Class to manage fitting process."""
        self.template = template
        self.losvd = losvd

    def __call__(self, method='leastsq', plot='bin', verbose='bin'):
        print('bal')
        pass
