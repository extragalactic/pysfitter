"""
This module provides a LSF (Line Spread Function) object which widely used
for the spectral fitting.
"""

from __future__ import print_function, division

import numpy as np
import lmfit
from matplotlib import pyplot as plt
from pysfitter import utils
from astropy.io import fits
from astropy import convolution
from scipy import signal, fftpack, optimize

from pysfitter import utils

# import matplotlib
# matplotlib.use('TkAgg')

def arc_resid(pars, x, data=None):
    const = pars['c'] + x*0.0
    sigma = pars['sigma']
    gau_amps = [v for k, v in pars.items() if k.startswith('amp')]
    gau_cnts = [v for k, v in pars.items() if k.startswith('cnt')]

    model = const
    comps = [const]
    for amp, cnt in zip(gau_amps, gau_cnts):
        cmp = amp*np.exp(-0.5*((x-cnt)/sigma)**2)
        model += cmp
        comps.append(cmp)

    if data is None:
        return model, comps
    else:
        return data - model


def arc_resid_moffat(pars, x, data=None):
    const = pars['c'] + x*0.0
    sigma = pars['sigma']
    beta = pars['beta']
    gau_amps = [v for k, v in pars.items() if k.startswith('amp')]
    gau_cnts = [v for k, v in pars.items() if k.startswith('cnt')]

    model = const
    comps = [const]
    for amp, cnt in zip(gau_amps, gau_cnts):
        cmp = amp*(1+((x-cnt)/sigma)**2)**(-beta)
        model += cmp
        comps.append(cmp)

    if data is None:
        return model, comps
    else:
        return data - model


def fit_arc(wave, spec, ngaus=20, init_xy=None, init_y=None, plot=False):
    smax = np.nanmax(spec)
    smin_nonzero = np.nanmin(spec[np.nonzero(spec)])
    if init_xy is None:
        gau_centers = np.linspace(np.min(wave), np.max(wave), ngaus)
        gau_amplitudes = np.ones(ngaus) * smax/2
    else:
        gau_centers = init_xy[0]
        gau_amplitudes = init_xy[1] - smin_nonzero
        ngaus = len(gau_centers)

    params = lmfit.Parameters()
    params.add(name='c', value=smin_nonzero, min=0, max=smin_nonzero, vary=False)
    params.add(name='sigma', value=2.2, min=0.1, max=10.0)
    for i in range(ngaus):
        suf = "{}".format(i)
        params.add(name='amp'+suf, value=gau_amplitudes[i], min=0, max=smax)
        params.add(name='cnt'+suf, value=gau_centers[i],
                   min=np.nanmin(wave), max=np.nanmax(wave))

    result = lmfit.minimize(arc_resid, params, args=(wave,),
                            kws=dict(data=spec), method='powell')

    print(lmfit.fit_report(result))
    bestfit, comps = arc_resid(result.params, wave)

    if plot:
        plt.plot(wave, spec)
        for cmp in comps:
            plt.plot(wave, cmp, ':')
        plt.plot(wave, bestfit, color='red')

        plt.plot(init_xy[0], init_xy[1], 'x')

        plt.show()

    return 0


def fit_arc_moffat(wave, spec, ngaus=20, init_xy=None, init_y=None, plot=False,
                   winwl=3.0):
    smax = np.nanmax(spec)
    smin_nonzero = np.nanmin(spec[np.nonzero(spec)])
    if init_xy is None:
        gau_centers = np.linspace(np.min(wave), np.max(wave), ngaus)
        gau_amplitudes = np.ones(ngaus) * smax/2
    else:
        gau_centers = init_xy[0]
        gau_amplitudes = init_xy[1] - smin_nonzero
        ngaus = len(gau_centers)

    params = lmfit.Parameters()
    params.add(name='c', value=smin_nonzero, min=0, max=smin_nonzero, vary=True)
    params.add(name='sigma', value=2.0, min=0.1, max=5.0, vary=True)
    params.add(name='beta', value=4.0, min=-1, max=5.0, vary=True)
    for i in range(ngaus):
        suf = "{}".format(i)
        params.add(name='amp'+suf, value=gau_amplitudes[i], min=0, vary=True)#, max=2*smax)
        params.add(name='cnt'+suf, value=gau_centers[i],
                   min=gau_centers[i]-winwl, max=gau_centers[i]+winwl)


    result = lmfit.minimize(arc_resid_moffat, params, args=(wave,),
                            kws=dict(data=spec), method='leastsq')

    print(lmfit.fit_report(result))
    bestfit, comps = arc_resid(result.params, wave)

    if plot:
        plt.plot(wave, spec)
        for cmp in comps:
            plt.plot(wave, cmp, ':')
        plt.plot(wave, bestfit, color='red')

        plt.plot(init_xy[0], init_xy[1], 'x')

        plt.show()

    return 0


def normalize_vector(vector, spec1d_normalized=1.0):
    """Normalize vector"""
    lowcut = np.nanpercentile(vector, [spec1d_normalized])
    vector_out = vector - lowcut
    rms = np.nanstd(vector_out)
    vector_out /= rms

    return vector_out


def multigaus(x, sigma, centers, fluxes):
    """Set of gaussians with the same sigma."""
    model = x*0.0
    for cnt, flx in zip(centers, fluxes):
        model += flx * np.exp(-0.5*((x-cnt)/sigma)**2)

    return model


def gaus_hermit(x, v, sigma, h3=0, h4=0, h5=0, h6=0, h7=0, h8=0):
    """Return Gauss-Hermite profile. Only h3, h4 coefficients are included."""

    w = (x-v)/sigma
    w2 = w**2
    losvd = np.exp(-0.5*w2)/(np.sqrt(2*np.pi)*sigma)

    polyn = 1.0 + (h3/np.sqrt(3.0)*(w*(2.0*w2 - 3.0)) +
                   h4/np.sqrt(24.0)*(w2*(4.0*w2-12.0)+3.0) +
                   h5/np.sqrt(60.0)*(w*(w2*(4.0*w2-20.0)+15.0)) +
                   h6/np.sqrt(720.0)*(w2*(w2*(8.0*w2-60.0)+90.0)-15.0) +
                   h7/np.sqrt(70.0)*(128.0*w**7-1344.0*w**5+3360.0*w**3-1680.0*w)/6720.0 +
                   h8/np.sqrt(70.0)*(256.0*w**8-3584.0*w**6+13440.0*w**4-13440.0*w2+1680.0)/26880.0
                   )
        # if npars eq 10 then $
        #     polyn=polyn + pars[8]*sqrt(35d)*(512d*w^9 - 9216d*w^7 + 48384d*w^5 - 80640d*w^3 + 30240d*w)/80640d $;H9
        #                 + pars[9]*sqrt(7d)*(1024d*w^10 - 23040d*w^8 + 161280d*w^6 - 403200d*w^4 + 302400d*w2 - 30240d)/161280d

    return losvd * polyn


def resid_acorr(pars, x, data=None):
    sigma = pars['sigma']
    h3 = pars['h3']
    h4 = pars['h4']

    nmax = int(sigma*10)
    xlosvd = np.linspace(-nmax, nmax, 2*nmax+1)

    losvd = gaus_hermit(xlosvd, 0.0, sigma, h3=h3, h4=h4)

    # calculate autocorrelation function
    model_acorr = signal.correlate(losvd, losvd, mode='same')
    model_acorr /= np.max(model_acorr)

    model = np.interp(x, xlosvd, model_acorr)

    if data is None:
        return model
    else:
        return data - model


def calculate_lsf_corpeak(spec, width=2.0, plot=False, verbose=False):
    """
    Calculate LSF shape based on fitting of autocorrelation peak of the
    arc spectrum fragment.

    Parameters
    ----------
    spec : 1d np.array
        Fragment of reference arc spectrum.
    width : float
        Typical value of line width in term of sigma.
    plot : bool
        Allow graphical output.
    verbose : type
        Allow rich test output.

    Returns
    -------
    type
        Description of returned object.
    """

    sz = spec.size
    peaks, _ = signal.find_peaks(spec, width=width, height=0)

    mask = np.ones(sz, dtype=bool)

    xspec = np.arange(sz)

    for pk in peaks:
        winmask = int(width*3)
        mask[pk-winmask:pk+winmask] = False

    background = np.nanmedian(spec[mask])
    spec_normalized = spec #- background

    window = np.hanning(sz)

    acorr = signal.correlate(spec_normalized*window, spec_normalized*window)
    maxcorr = np.max(acorr)
    acorr /= maxcorr
    central_pixel = int((acorr.size-1)/2)
    xacorr = np.arange(acorr.size) - central_pixel

    params = lmfit.Parameters()
    params.add(name='sigma', value=2.4, min=1.0, max=5.0)
    params.add(name='h3', value=0.0, min=-0.3, max=0.3, vary=False)
    params.add(name='h4', value=0.0, min=-0.3, max=0.3, vary=False)

    mask_peak = np.abs(xacorr) < width*3

    result = lmfit.minimize(resid_acorr, params, method='leastsq',
                            args=(xacorr[mask_peak],),
                            kws=dict(data=acorr[mask_peak]))

    if verbose:
        print(lmfit.fit_report(result))

    model_acorr = resid_acorr(result.params, xacorr)

    if plot:
        plt.subplot(2,1,1)
        plt.plot(xacorr, acorr, 'C0')
        plt.plot(xacorr[mask_peak], acorr[mask_peak], 'C0o')
        plt.plot(xacorr, model_acorr, color='red')
        plt.xlim(-width*50, width*50)
        plt.subplot(2,1,2)
        plt.plot(xacorr[mask_peak], acorr[mask_peak]-model_acorr[mask_peak], 'o')
        plt.axhline(0)
        plt.xlim(-width*50, width*50)
        plt.pause(1.0)

    return result.params['sigma'], result.params['h3'], result.params['h4']


def resid_fft2(pars, x, data=None):
    bkg = pars['bkg']
    I0 = pars['int']
    sigma = pars['sigma']
    h3 = pars['h3']
    h4 = pars['h4']
    h5 = pars['h5']
    h6 = pars['h6']

    sz = x.size
    losvd = I0*gaus_hermit(x, sz/2, sigma, h3=h3, h4=h4, h5=h5, h6=h6) + bkg
    losvd_fft = fftpack.fft(losvd)
    model = np.abs(losvd_fft)

    if data is None:
        return model, losvd
    else:
        return data - model


def line_profile(x, p):
    """Line profile parametrization which better explain shape of arc lines."""
    x0, i0, sigma, alpha = p
    y = np.abs((x-x0)/sigma)
    return i0*np.exp(-0.5*y**alpha)


def resid_fft2(pars, x, data_fft_abs=None, data=None, peaks=None):
    bkg = pars['bkg']
    I0 = pars['int']
    sigma = pars['sigma']
    alpha = pars['h3']
    h4 = pars['h4']
    h5 = pars['h5']
    h6 = pars['h6']

    sz = x.size
    if peaks is None:
        # gh = gaus_hermit(x, sz/2, sigma, h3=h3, h4=h4, h5=h5, h6=h6)
        model = line_profile(x, [sz/2, I0, sigma, alpha]) + bkg
    else:
        model = data*0.0 + bkg
        for pk in peaks:
            I0 = data[pk] - bkg
            model += line_profile(x, [pk, I0, sigma, alpha])

    model_fft = fftpack.fft(model)
    model_abs = np.abs(model_fft)

    if data_fft_abs is None:
        return model_abs, model
    else:
        return data_fft_abs - model_abs


def calculate_lsf_fft2(spec, width=2.0, plot=False, verbose=False):
    """
    Calculate LSF shape by fitting Foruier image of the given arc spectrum
    fragment.

    Parameters
    ----------
    spec : 1d np.array
        Fragment of reference arc spectrum.
    width : float
        Typical value of line width in term of sigma.
    plot : bool
        Allow graphical output.
    verbose : type
        Allow rich test output.

    Returns
    -------
    type
        Description of returned object.
    """

    sz = spec.size

    peaks, _ = signal.find_peaks(spec, width=width, height=np.nanmedian(spec))

    fft = fftpack.fft(spec)
    fft_abs = np.abs(fft)
    xspec = np.arange(sz)

    params = lmfit.Parameters()
    params.add(name='bkg', value=np.nanmedian(spec), min=0, max=1e5, vary=False)
    params.add(name='int', value=1.0, min=0, max=1e5)
    params.add(name='sigma', value=2.4, min=0.1, max=10.0)
    params.add(name='h3', value=2, min=1, max=5, vary=True)
    params.add(name='h4', value=0.0, min=-0.3, max=0.3, vary=False)
    params.add(name='h5', value=0.0, min=-0.3, max=0.3, vary=False)
    params.add(name='h6', value=0.0, min=-0.3, max=0.3, vary=False)

    result0 = lmfit.minimize(resid_fft2, params, method='powell',
                             args=(xspec,),
                             kws=dict(data_fft_abs=fft_abs, data=spec, peaks=peaks))
    result = lmfit.minimize(resid_fft2, result0.params, method='leastsq',
                            args=(xspec,),
                            kws=dict(data_fft_abs=fft_abs, data=spec, peaks=peaks))

    if verbose:
        print(lmfit.fit_report(result0))
        print(lmfit.fit_report(result))

    model_fft_abs, model = resid_fft2(result.params, xspec, data=spec, peaks=peaks)

    if plot:
        plt.subplot(2,1,1)
        plt.plot(spec, 'C0')
        plt.plot(model, 'C1')

        fft_abs = fftpack.fftshift(fft_abs)
        model_fft_abs = fftpack.fftshift(model_fft_abs)
        plt.subplot(2,1,2)
        plt.plot(fft_abs, 'C0')
        plt.plot(model_fft_abs, 'C1')
        plt.plot(fft_abs-model_fft_abs, '+', ms=1)
        plt.show()

    return result.params['sigma'], result.params['h3'], result.params['h4']


def resid_fft3(pars, x, data_fft_abs=None, data=None, mode='fit'):
    bkg = pars['bkg']
    sigma = pars['sigma']
    alpha = pars['alpha']
    # amps = [v for k, v in pars.items() if k.startswith('amp')]
    cnts = [v for k, v in pars.items() if k.startswith('cnt')]

    model_matrix = np.zeros((data.size, len(cnts)))
    for idx, cnt in enumerate(cnts):
        model_matrix[:, idx] = line_profile(x, [cnt, 1.0, sigma, alpha])

    # sol = optimize.lsq_linear(model_matrix, data, method='bvls')
    # amplitudes = sol.x
    amplitudes, _ = optimize.nnls(model_matrix, data - bkg)

    model = np.dot(model_matrix, amplitudes)
    model += bkg

    if mode is 'model':
        return model
    elif mode is 'fit':
        return data - model


def resid_fft3_iter_cb(params, iter, resid, *arg, **kws):
    print("iter {}".format(iter))
    print("sig={:.2f}; alp={:.2f}; bkg={:.2f}".format(params['sigma'].value, params['alpha'].value, params['bkg'].value))
    x = arg[0]
    data = kws['data']
    mode = kws['mode']

    model = resid_fft3(params, x, data=data, mode='model')

    if 'plot' in kws:
        plt.clf()
        plt.plot(data)
        plt.plot(model)
        plt.draw()
        plt.pause(0.01)


def calculate_lsf_fft3(spec, width=2.0, plot=False, verbose=False):
    """
    Calculate LSF shape by fitting Fourier image of the given arc spectrum
    fragment.

    Parameters
    ----------
    spec : 1d np.array
        Fragment of reference arc spectrum.
    width : float
        Typical value of line width in term of sigma.
    plot : bool
        Allow graphical output.
    verbose : type
        Allow rich test output.

    Returns
    -------
    type
        Description of returned object.
    """

    sz = spec.size

    peaks, _ = signal.find_peaks(spec, width=width, height=0)

    fft = fftpack.fft(spec)
    fft_abs = np.abs(fft)
    xspec = np.arange(sz)

    params = lmfit.Parameters()
    params.add(name='bkg', value=0.0, min=0, max=np.nanmedian(spec), vary=True)
    params.add(name='sigma', value=2.4, min=0.1, max=10.0)
    params.add(name='alpha', value=2, min=1, max=5, vary=True)
    for id, pk in enumerate(peaks):
        suf = "{}".format(id)
        # params.add("amp"+suf, value=spec[pk], min=0, max=np.nanmax(spec))
        params.add("cnt"+suf, value=pk, min=pk-width/2, max=pk+width/2)

    kws = dict(data_fft_abs=fft_abs, data=spec, mode='fit')
    result0 = lmfit.minimize(resid_fft3, params, method='powell',
                             args=(xspec,),
                             kws=kws,
                             iter_cb=None)#resid_fft3_iter_cb)
    result = lmfit.minimize(resid_fft3, result0.params, method='leastsq',
                            args=(xspec,),
                            kws=kws)

    if verbose:
        print(lmfit.fit_report(result0))
        print(lmfit.fit_report(result))

    model = resid_fft3(result.params, xspec, data=spec, mode='model')

    if plot:
        plt.cla()
        plt.subplot(1,1,1)
        plt.plot(spec, 'C0')
        plt.plot(model, 'C1')
        plt.pause(0.1)
        # plt.show()

    return result.params['sigma'], result.params['alpha']


file = '/Users/ik52/obs/salt/Rings/20180607/2018-1-MLT-003/Reduction/mbxgpP201806070048eew.fits.gz'
file = '/Users/ik52/obs/salt/Rings/20180612/2018-1-MLT-003/Reduction/mbxgpP201806120066eew.fits.gz'
file = '/Users/ik52/obs/salt/n3273/mbxgpP201505180045eew.fits'

llist = '/Users/ik52/obs/processed/salt/arc_analysis/arc_reference/Ar_nist_corrected.txt'
llist = 'Xe.txt'
hdu = fits.open(file)
hdu.info()

wlist = np.genfromtxt(llist)

spec = hdu[0].data
header = hdu[0].header
size = hdu[0].shape
win0 = 0.5# in angstrom

wrange, waves = utils.get_waves(header)
cdelt = waves[1] - waves[0]

win_segment = 151
nsegments = 50
grid = np.linspace(0, size[1]-1 - win_segment, nsegments, dtype=int)
lambdas = np.zeros(nsegments)
sigmas = np.zeros(nsegments)
alphas = np.zeros(nsegments)

for q in range(nsegments):
# for q in [0]:
    spec1d = np.median(spec[:, grid[q]:grid[q]+win_segment], axis=0)
    wave1d = waves[grid[q]:grid[q]+win_segment]

    msk = np.ones(spec1d.size, dtype=bool)
    # msk[((spec1d == 0) | (wave1d <= 4200))] = False
    msk[(spec1d == 0)] = False

    sigma, alpha = calculate_lsf_fft3(spec1d[msk], plot=True, verbose=True)

    lambdas[q] = np.median(wave1d)
    sigmas[q] = sigma.value
    alphas[q] = alpha.value

    print("wave, sig, alph: {:g} - {:g} - {:g}".format(lambdas[q], sigma.value,
                                                       alpha.value))
