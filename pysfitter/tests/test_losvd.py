from pysfitter import losvd
import lmfit
import numpy as np

def test_default_moments():
    gh = losvd.GaussHermite()
    assert gh.moments == 6


def test_default_length_names():
    gh = losvd.GaussHermite()
    assert gh.moments == len(gh.names)


def test_default_length_values():
    gh = losvd.GaussHermite()
    assert gh.moments == len(gh.values)


def test_gaussian_moments():
    gh = losvd.GaussHermite(values=[0.0, 100.0])
    assert gh.moments == 2


def test_gaussian_length_names():
    gh = losvd.GaussHermite(values=[0.0, 100.0])
    assert gh.moments == len(gh.names)


def test_parameters_instance():
    gh = losvd.GaussHermite()
    assert isinstance(gh.get_params(), lmfit.Parameters)


def test_get_profile_length():
    gh = losvd.GaussHermite()
    vbin = np.linspace(-500, 500)
    profile = gh.get_profile(vbin)
    assert len(profile) == len(vbin)


def test_get_profile_check_tails():
    gh = losvd.GaussHermite(values=[0.0, 10.0])
    vbin = np.linspace(-50, 50)
    profile = gh.get_profile(vbin)
    np.testing.assert_array_almost_equal(profile[[0,-1]], np.array([0.0, 0.0]))
