from __future__ import print_function, division

import numpy as np
from scipy import interpolate
from astropy.convolution import convolve

from matplotlib import pyplot as plt


def interpolator_spline_prep(grid, ages, mets, mgfes, log=True):

    if log:
        flx = np.log10( grid )
    else:
        flx = grid

    interp_coefs = []
    for i in range(grid.shape[-1]):
        ispl1 = interpolate.RectBivariateSpline(mets, ages, flx[0,:,:,i])
        ispl2 = interpolate.RectBivariateSpline(mets, ages, flx[1,:,:,i])
        interp_coefs.append( (ispl1, ispl2) )
    return interp_coefs


# def interpolator_spline_eval(interp_coefs, age, met, mgfe, log=True, jac=False):
#     sz = len(interp_coefs)
#     spec = np.zeros((sz,2))
#
#     if jac:
#         spec_dZ = np.zeros((sz,2))
#         spec_dT = np.zeros((sz,2))
#
#     weight_mgfe = np.array([1.0 - mgfe/0.4, mgfe/0.4])
#     for i, (ispl1, ispl2) in enumerate(interp_coefs):
#         spec[i,0] = ispl1(met, age)
#         spec[i,1] = ispl2(met, age)
#         if jac:
#             if log:
#                 spec_dZ[i,0] = ispl1(met, age, dx=0, dy=1) * met / np.log(10)
#                 spec_dZ[i,1] = ispl2(met, age, dx=0, dy=1) * met / np.log(10)
#                 spec_dT[i,0] = ispl1(met, age, dx=1, dy=0) * age / np.log(10)
#                 spec_dT[i,1] = ispl2(met, age, dx=1, dy=0) * age / np.log(10)
#             else:
#                 spec_dZ[i,0] = ispl1(met, age, dx=0, dy=1)
#                 spec_dZ[i,1] = ispl2(met, age, dx=0, dy=1)
#                 spec_dT[i,0] = ispl1(met, age, dx=1, dy=0)
#                 spec_dT[i,1] = ispl2(met, age, dx=1, dy=0)
#     if log:
#         spec_out = np.power(10.0, np.dot(spec, weight_mgfe))
#     else:
#         spec_out = np.dot(spec, weight_mgfe)
#
#     if jac:
#         spec_out_dT = np.dot(spec_dT, weight_mgfe)
#         spec_out_dZ = np.dot(spec_dZ, weight_mgfe)
#         if log:
#             spec_out_dA = (spec[:,1]-spec[:,0]) / 0.4 * mgfe / np.log(10)
#         else:
#             spec_out_dA = (spec[:,1]-spec[:,0]) / 0.4
#
#         return spec_out, spec_out_dT, spec_out_dZ, spec_out_dA
#     else:
#         return spec_out


def logrebin(spec, waves, velscale=20.0, kind='cubic', outwaves=True):
    """
    This function to rebin input spectrum to logarithmic wavelengths for given
    velscale parameter.
    """
    log_waves = np.exp( np.arange( np.log(np.min(waves)), np.log(np.max(waves)) , velscale/299792.45) )
    fnc = interpolate.interp1d(waves, spec, kind=kind, fill_value='extrapolate')
    log_spec = fnc(log_waves)

    if outwaves:
        return log_spec, log_waves
    else:
        return log_spec


def conv_gaus(spec, vel, sig, jac=False):
    nn = np.ceil(np.abs(vel) + 5 * sig)
    x = np.linspace(-nn, nn, 2*nn+1)
    yy = (x-vel)/sig
    kern = 1.0 / np.sqrt(2*np.pi) / sig * np.exp(-0.5*yy**2)
    if not jac:
        return convolve(spec, kern, normalize_kernel=False)
    else:
        expo = np.exp(-0.5*yy**2)
        kern2 = 1.0 / np.sqrt(2*np.pi) * ( expo * (x-vel)/sig**3 - expo / sig**2)
        return convolve(spec, kern2, normalize_kernel=False)


def resid_spec_fit_Dfun(p, spec, wave_msk, grid, grid_params, grid_waves, velscale, spl, mode='spline', plotcb=False):
    if mode == 'spline':
        model_spec, dT, dZ, dA = interpolator_spline_eval(spl, p['age'], p['met'], p['mgfe'], jac=True)
    elif mode == 'linear':
        # TO BE WRITEN
        model_spec = interpolate_linear( grid, grid_params, grid_waves, [p['mgfe'], p['met'], p['age']])

    model_spec_conv = conv_gaus(model_spec, p['vel']/velscale, p['sig']/velscale)

    xx = np.linspace(-1, 1, spec.size)
    coefs = [p[pr].value for pr in p if 'mpoly' in pr]
    mpoly = np.polynomial.legendre.legval(xx, coefs)

    jac = np.zeros( (len(spec), len(p)) )
    ddT = mpoly * conv_gaus(dT, p['vel']/velscale, p['sig']/velscale)[wave_msk]
    ddZ = mpoly * conv_gaus(dZ, p['vel']/velscale, p['sig']/velscale)[wave_msk]
    ddA = mpoly * conv_gaus(dA, p['vel']/velscale, p['sig']/velscale)[wave_msk]
    ddv = mpoly * (model_spec_conv * velscale / p['sig'])[wave_msk]
    dds = mpoly * conv_gaus(model_spec, p['vel']/velscale, p['sig']/velscale, jac=True)[wave_msk] * velscale
    jac[:,0] = ddT
    jac[:,1] = ddZ
    jac[:,2] = ddA
    jac[:,3] = ddv
    jac[:,4] = dds
    for q in range(len(coefs)):
        coef_zeros = np.zeros(len(coefs))
        coef_zeros[q] = 1.0
        jac[:,5+q] = np.polynomial.legendre.legval(xx, coef_zeros)

    return -jac


def resid_spec_fit(p, spec, wave_msk, grid, grid_params, grid_waves, velscale, spl, mode='spline', plotcb=False):
    if mode == 'spline':
        model_spec = interpolator_spline_eval(spl, p['age'], p['met'], p['mgfe'])
    elif mode == 'linear':
        model_spec = interpolate_linear( grid, grid_params, grid_waves, [p['mgfe'], p['met'], p['age']])

    model_spec_conv = conv_gaus(model_spec, p['vel']/velscale, p['sig']/velscale)

    xx = np.linspace(-1, 1, spec.size)
    coefs = [p[pr].value for pr in p if 'mpoly' in pr]
    mpoly = np.polynomial.legendre.legval(xx, coefs)

    return spec - mpoly*model_spec_conv[wave_msk]


def resid_spec_fit_cb(p, iter, resid, spec, wave_msk, grid, grid_params, grid_waves, velscale, spl, mode='spline', plotcb=False):
    # print("===================================================================")
    pr = ( p['vel'].value, p['sig'].value, 10**p['age'].value, p['met'].value, p['mgfe'].value)
    if (iter % 10) == 0:
        print("Iter {}; v={:7.2f}; s={:7.2f}; T={:4.3f}, Z={:5.3f}; A={:5.3f}; chi2={:e}".format(
            iter, pr[0], pr[1], pr[2], pr[3], pr[4], np.sum(resid**2)))
        # coefs = [p[pr].value for pr in p if 'mpoly' in pr]
        # print("Coefs mpoly:", coefs)

    if plotcb:
        if (iter % 10) == 0:
            xx = np.linspace(-1, 1, spec.size)
            coefs = [p[pr].value for pr in p if 'mpoly' in pr]
            mpoly = np.polynomial.legendre.legval(xx, coefs)
            plt.clf()
            plt.plot(grid_waves[wave_msk], spec)
            plt.plot(grid_waves[wave_msk], spec-resid, color='red')
            plt.plot(grid_waves[wave_msk], resid, 'k+', ms=3)
            plt.plot(grid_waves[wave_msk], mpoly/np.median(mpoly), lw=0.5)
            plt.pause(0.0001)
