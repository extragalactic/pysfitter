from __future__ import print_function, division

import numpy as np
from astropy.convolution import convolve
import glob
from astropy.io import fits
from pysfitter import settings

path = settings.PYSFITTER_PATH


def convert_models_miles(writeto='miles.fits'):
    """
    Convert original MILES model file to one fits file.
    """

    file_list = glob.glob(path + 'models/miles_v11/BASTI_KU_Ep0.?0/*.fits')
    nfiles = len(file_list)

    pattern = re.compile(r'KU_Ep(\d.\d{2})/M(\w{2})(\d.\d{2})Z(\w)(\d.\d{2})T(\d{2}.\d{4})')

    params = np.zeros( nfiles, dtype=[('alpha', float), ('met', float), ('age', float)])

    for i, file in enumerate(file_list):

        alpha, imf_type, imf_slope, met_sign, met, age = pattern.findall(file)[0]
        params['alpha'][i] = float(alpha)
        if met_sign == 'm':
            params['met'][i] = -float(met)
        else:
            params['met'][i] = float(met)
        params['age'][i] = float(age)

        # print(i, params[i])
        # if i % 10:
            # utils.update_progress(i, len(file_list))

    ages = np.unique(params['age'])
    mets = np.unique(params['met'])
    alps = np.unique(params['alpha'])

    for i, file in enumerate(file_list):
        hdu = fits.open(file)
        hdr = hdu[0].header
        range, waves = get_waves(hdr)

        if i == 0:
            models = np.zeros((waves.size, ages.size, mets.size, alps.size))

        idx_age = np.where( ages == params['age'][i] )[0][0]
        idx_met = np.where( mets == params['met'][i] )[0][0]
        idx_alp = np.where( alps == params['alpha'][i] )[0][0]
        models[:,idx_age, idx_met, idx_alp] = hdu[0].data

        if i % 10:
            utils.update_progress(i, len(file_list))

    if writeto:
        # write prymary extension with model grid
        hdr['CTYPE1'] = 'AWAV'
        hdu_prim = fits.PrimaryHDU(models.T, hdr)

        # write binary table extension with grid parameters
        col_wave = fits.Column(name='wave', array=[waves],
            format="{}D".format(waves.size), unit='Angstrom')
        col_age = fits.Column(name='age', array=[ages],
            format="{}E".format(ages.size), unit='Gyr')
        col_met = fits.Column(name='met', array=[mets],
            format="{}E".format(mets.size), unit='dex')
        col_mgf = fits.Column(name='mgfe', array=[alps],
            format="{}E".format(alps.size), unit='dex')
        hdu_tbl = fits.BinTableHDU.from_columns([col_wave, col_age, col_met, col_mgf])
        hdu_tbl.name = 'PARAMS'

        fits.HDUList([hdu_prim, hdu_tbl]).writeto(writeto, overwrite=True)


    return models, waves, ages, mets, alps


def convert_models_pegase(writeto='pegase.fits', input_pattern='models/pegase/*.fits'):
    """
    Convert output files of Pegase.HR stellar population code to the one fits
    with entire grid.
    """

    file_list = glob.glob(path + input_pattern)
    nfiles = len(file_list)
    hdu = fits.open(file_list[0])
    nrows = hdu['ETS_PARA'].data.shape[0]
    hdu.close()
    params = np.zeros(nfiles*nrows, dtype=[('met', float), ('age', float)])

    for i, file in enumerate(file_list):
        hdu = fits.open(file)
        n_rows = hdu[2].data.shape[0]
        params['age'][i:(i+n_rows)] = np.round(hdu[2].data['AGE']/1e3,4)
        params['met'][i:(i+n_rows)] = np.round(np.log10(hdu[2].data['Zstars']/0.02),2)
        hdu.close()
    ages = np.unique(params['age'][params['age'] != 0.])
    mets = np.unique(params['met'])

    for i, file in enumerate(file_list):
            hdu = fits.open(file)
            hdr = hdu[0].header
            range, waves = get_waves(hdr)
            if i == 0:
                models = np.zeros((waves.size, ages.size, mets.size))
            for j, spectrum in enumerate(hdu[0].data):
                idx_age = np.where( np.abs(ages-hdu[2].data[j]['AGE']/1e3) < 0.001)[0][0]
                idx_met = np.where( np.abs(mets - np.round(np.log10(hdu[2].data[j]['Zstars']/0.02),2)) < 0.001)[0][0]
                models[:,idx_age, idx_met] = spectrum

    if writeto:
        # write prymary extension with model grid
        hdr['CTYPE1'] = 'AWAV'
        hdu_prim = fits.PrimaryHDU(models.T, hdr)

        # write binary table extension with grid parameters
        col_wave = fits.Column(name='wave', array=[waves],
            format="{}D".format(waves.size), unit='Angstrom')
        col_age = fits.Column(name='age', array=[ages],
            format="{}E".format(ages.size), unit='Gyr')
        col_met = fits.Column(name='met', array=[mets],
            format="{}E".format(mets.size), unit='dex')
        hdu_tbl = fits.BinTableHDU.from_columns([col_wave, col_age, col_met])
        hdu_tbl.name = 'PARAMS'

        fits.HDUList([hdu_prim, hdu_tbl]).writeto(writeto, overwrite=True)


    return models, waves, ages, mets


def get_waves(hdr):
    """
    Output params:
    @param range - wavelength range
    @param waves - wavelengths
    """
    waves = hdr['CRVAL1'] - (hdr['CRPIX1']-1.0)*hdr['CDELT1'] + ( np.arange(0., hdr['NAXIS1']) ) * hdr['CDELT1']
    ran = [waves[0], waves[-1]]
    return ran, waves


def conv_gaus(spec, vel, sig):
    """
    Convolve spectrum with gaussian kernel.
    """
    nn = np.ceil(np.abs(vel) + 5 * sig)
    x = np.linspace(-nn, nn, 2*nn+1)
    yy = (x-vel)/sig
    kern = 1.0 / np.sqrt(2*np.pi) / sig * np.exp(-0.5*yy**2)

    return convolve(spec, kern, normalize_kernel=False)
