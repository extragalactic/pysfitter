"""
This module provides a LOSVD (Line of Sight Velocisty Distribution) object
which used as a convolution kernel in the spectral fitting.
"""
from lmfit import Parameters
from astropy import convolution
import numpy as np


def gaus_hermit(x, v, sigma, h3=0, h4=0, h5=0, h6=0, h7=0, h8=0):
    """Return Gauss-Hermite profile"""

    w = (x-v)/sigma
    w2 = w**2
    losvd = np.exp(-0.5*w2)/(np.sqrt(2*np.pi)*sigma)

    polyn = 1.0 + (h3/np.sqrt(3.0)*(w*(2.0*w2 - 3.0)) +
                   h4/np.sqrt(24.0)*(w2*(4.0*w2-12.0)+3.0)
                   + h5/np.sqrt(60.0)*(w*(w2*(4.0*w2-20.0)+15.0))
                   + h6/np.sqrt(720.0)*(w2*(w2*(8.0*w2-60.0)+90.0)-15.0)
                   + h7/np.sqrt(70.0)*(128.0*w**7-1344.0*w
                                       ** 5+3360.0*w**3-1680.0*w)/6720.0
                   + h8/np.sqrt(70.0)*(256.0*w**8-3584.0*w**6
                                       + 13440.0*w**4-13440.0*w2+1680.0)/26880.0
                   )

    return losvd * polyn


class GaussHermite(object):
    def __init__(self,
                 # names=['velocity', 'dispersion', 'h3', 'h4', 'h5', 'h6'],
                 names=['velocity', 'dispersion', 'h3', 'h4', 'h5', 'h6'],
                 values=[0.0, 100.0, 0.0, 0.0, 0.0, 0.0],
                 limits=[[-500.0, 500.0], [10.0, 500.0], [-0.3, 0.3],
                         [-0.3, 0.3], [-0.2, 0.2], [-0.2, 0.2]],
                 vary=[True, True, True, True, False, False],
                 ):
        """
        This class describes a Gauss-Hermite parametrization of the spectral
        lines up to 6th order. This parametrization has been proposed by
        `van der Marel & Franx (1993) <https://ui.adsabs.harvard.edu/#abs/1993ApJ...407..525V/abstract>`_
        and `Gerhard (1993) <https://ui.adsabs.harvard.edu/#abs/1993MNRAS.265..213G/abstract>`_.

        Parameters of this parametrization are `velocity`, `dispersion`, `h3`,
        `h4`, `h5`, `h6`. This sequence should not be changed.

        One can controll shape of the line providing different number of
        values in the `values`. For instance, `values=[0,100]` corresponds to
        pure Gausssian line shape.

        `values`, `limits`, `vary` will be passed to LMFIT for minimization.

        Parameters
        ----------
        names : sequence of str
            Names of the parameters.
        values : sequence of floats
            Initial guesses for LOSVD parametrization parameters. The length of
            `values` specify final shape of LOSVD. Two-elements in `values`
            correspond to pure Gaussian shape, 4-elements includes `h3`, `h4`,
            6-elements add `h5` and `h6`.
        """

        # number of moments in the parametrization
        self.moments = len(values)

        self.names = names[:self.moments]
        self.values = values[:self.moments]
        self.limits = limits[:self.moments]
        self.vary = vary[:self.moments]

    def get_params(self):
        """Return set of LOSVD parameters as an LMFIT parameters set"""

        params = Parameters()
        for name, value, limit, var in zip(self.names, self.values,
                                           self.limits, self.vary):
            if name is 'velocity':
                params.add(name, value=value, min=value+limit[0],
                           max=value+limit[1], vary=var)
            else:
                params.add(name, value=value, min=limit[0],
                           max=limit[1], vary=var)
        return params

    def set_params(self, params):
        """Take required parameters from LMFIT params set."""

        for id, name in enumerate(self.names):
            self.values[id] = params[name].value

    def get_profile(self, x):
        """Calculate LOSVD profile based on provided parameters."""

        if self.moments is 2:
            profile = gaus_hermit(x, self.values[0], self.values[1])
        elif self.moments is 4:
            profile = gaus_hermit(x, self.values[0], self.values[1],
                                  h3=self.values[2], h4=self.values[3])
        elif self.moments is 6:
            profile = gaus_hermit(x, self.values[0], self.values[1],
                                  h3=self.values[2], h4=self.values[3],
                                  h5=self.values[4], h6=self.values[5])

        return profile

    def get_kernel(self, velscale=20.0):
        """Prepare convolution kernel for fitting routing."""

        velocity = self.values[0]
        sigma = self.values[1]
        vmax = np.abs(velocity) + 5*sigma
        npix = int(vmax/velscale)
        vbin = np.linspace(-vmax, vmax, 2*npix+1)

        kernel = convolution.CustomKernel(self.get_profile(vbin)*velscale)
        return kernel
