from __future__ import print_function, division

import numpy as np
from astropy.io import fits
from scipy import interpolate, linalg
from scipy.spatial import Delaunay

from pysfitter import settings, Spectrum

path = settings.PYSFITTER_PATH


class Grid(object):
    """
    Base class for inteprolation of the spectra from  of stellar population
    model grids.

    Parameters
    ----------
    gridfile : str
        Path to the fits file with the grid.
    waves_on : :class: `pysfitter.Spectrum or array_like` or None:
        Describe information about wavelengths which will be used for
        interpolation of the grid.
    velscale : float
        Size of pixel in logrebinned spectrum in terms of km/s.
    norm_degree :  int or None
        If equal 1 then each spectrum in the grid normalized by their
        median value. If ``norm_degree>1`` then spectrum approximated by
        Legendre polynomial of the given order. This trick significantly
        improves convergence of the minimization because it partly suppress
        parameter degeneracy between continuum parameters and others. See
        details in documentation.
    vsys : float
        Approximation value for the line-of-sight velocity. It used to
        shift the grid spectra to given redshift.
    vamp : float
        Amplitude in the LOS velocity.
    samp : float
        Amplitude in the velocity dispersion. ``vamp`` and ``samp`` are
        used to calculate pad around wavelengths of the observed spectrum.
        It is required to avoid marginal effects during minimization.
    """

    def __init__(self, waves_on=None,
                 velscale=20.0, norm_degree=1.0, vsys=5000.0, vamp=600.0,
                 samp=400.0):

        #: 4-dimentional :class:`numpy.ndarray` array with MILES model grid.
        self.grid = None

        #: array_like : Wavelengths of the model spectra.
        self.waves = None

        #: float : Size of pixel in logrebinned spectrum in terms of km/s.
        self.velscale = velscale

        self.waves_msk = None
        """
        Preparing the MILES grid the spectral range is covered observed
        spectrum with plenty of value. ``waves_msk`` is masked from
        extended wavelength only overlapped with observed spectrum.
        """

        self.ages = None  #: Ages in Myr of corresponding nodes of grid.
        self.mets = None  #: Stellar metallicities in dex.
        self.mgfes = None  # [Mg/Fe] values in dex.

        #: See `pysfitter.InterpolatorMiles.norm_degree`
        self.norm_degree = norm_degree

        self._read_grid(path+self.gridfile)

        # make re-interpolation, log-binning and normalization
        # TODO make easy and based on comaprison with Spectrum object ot None
        if waves_on is None:
            # no information about new wavelengths
            self.prepare_range(self.waves, velscale=velscale,
                               norm_degree=norm_degree, vsys=vsys, vamp=vamp,
                               samp=samp)
        elif type(waves_on) is Spectrum:
            self.prepare_range(waves_on.waves, velscale=waves_on.velscale,
                               norm_degree=norm_degree, vsys=vsys, vamp=vamp,
                               samp=samp)
        else:
            raise TypeError("""`waves_on` have to be None or
                            `pysfitter.Spectrum` object""")

        # self.coefficients = self._calculate_coefficients()

    def _read_grid(self, file):
        """
        Method to read MILES models grid.
        """
        hdu = fits.open(file)
        grid = hdu[0].data
        grid_params = hdu[1].data

        self.grid = grid
        self.waves = grid_params[0]['wave']

        self.ages = grid_params[0]['age']
        self.lages = np.log10(grid_params[0]['age'])
        self.mets = grid_params[0]['met']
        self.mgfes = grid_params[0]['mgfe']

    def prepare_range(self, waves_spec, velscale=20.0, norm_degree=1,
                      vsys=5000.0, vamp=600.0, samp=400.0):
        """
        Main method to inteprolate, log-rebinned and re-normalized model
        spectra.
        """
        # reinterpolate to given wave range and velscale
        n_pix_ext = np.round((vamp + 3 * samp) / velscale).astype(np.int)
        waves_new = np.exp(np.pad(np.log(waves_spec), n_pix_ext,
                                  'reflect', reflect_type='odd'))
        waves_msk = ((waves_new.argsort() >= n_pix_ext) &
                     (waves_new.argsort() <= waves_new.size - n_pix_ext - 1))

        fnc_grid = interpolate.interp1d(self.waves, self.grid, kind='cubic',
                                        fill_value='extrapolate')
        grid_new = fnc_grid(waves_new * (1.0 - vsys / 299792.45))

        # renormilized each spectrum by median value
        if not norm_degree is None:
            xpol = np.linspace(-1, 1, grid_new.shape[-1])
            for i in range(grid_new.shape[0]):
                for j in range(grid_new.shape[1]):
                    for k in range(grid_new.shape[2]):
                        if norm_degree == 1:
                            # Normalized spectra by median value in each spectrum
                            grid_new[i, j, k, :] /= np.nanmedian(grid_new[i, j, k, :])
                        elif norm_degree > 1:
                            # Normalized spectra by low-order polinomials. It
                            # helps to avoid degeneracies between SSP parameters
                            # and multiplicativ cintinuum.
                            cpol = np.polynomial.Legendre.fit(xpol, grid_new[i, j, k, :], norm_degree)
                            grid_new[i, j, k, :] /= np.polynomial.legendre.legval(xpol, cpol.coef)

        self.grid = grid_new
        self.waves = waves_new
        self.waves_msk = waves_msk
        self.velscale = velscale


class SplineInterpolator(Grid):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.coefficients = self._calculate_coefficients()

    def _calculate_coefficients(self, log=True):
        """
        Method to calculate coefficients of spline representation for given
        wavelengths.
        """
        if log:
            flx = np.log10(self.grid)
        else:
            flx = self.grid

        interp_coefs = []
        for i in range(flx.shape[-1]):
            ispl1 = interpolate.RectBivariateSpline(self.mets,
                                                    self.lages,
                                                    flx[0, :, :, i])
            ispl2 = interpolate.RectBivariateSpline(self.mets,
                                                    self.lages,
                                                    flx[1, :, :, i])
            interp_coefs.append((ispl1, ispl2))

        return np.array(interp_coefs)

    def eval(self, lage, met, mgfe, log=True):
        """
        This is main method of interpolator which return evaluated model
        spectrum for given parameters.
        """
        sz = len(self.coefficients)
        spec = np.zeros((sz, 2))

        weight_mgfe = np.array([1.0 - mgfe / 0.4, mgfe / 0.4])
        for i, (ispl1, ispl2) in enumerate(self.coefficients):
            spec[i, 0] = ispl1(met, lage)
            spec[i, 1] = ispl2(met, lage)

        return 10**np.dot(spec, weight_mgfe)


class TriangulationInterpolator(Grid):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.coefficients = self._calculate_coefficients()

    def _calculate_coefficients(self):
        """
        Method to calculate coefficients of spline representation for given
        wavelengths.
        """
        # stupid recalculation of multi-dimensional grid to 2d which is much
        # more usefull for Delaunay triangulation.
        nspectra = self.mgfes.size * self.mets.size * self.lages.size
        nwaves = self.grid.shape[-1]
        new_grid = np.zeros((nspectra, nwaves))
        new_mgfes = np.zeros(nspectra)
        new_mets = np.zeros(nspectra)
        new_lages = np.zeros(nspectra)
        cnt = 0
        for imgfe, mgfe in enumerate(self.mgfes):
            for imet, met in enumerate(self.mets):
                for ilage, lage in enumerate(self.lages):
                    new_grid[cnt, :] = self.grid[imgfe, imet, ilage, :]
                    new_mgfes[cnt] = mgfe
                    new_mets[cnt] = met
                    new_lages[cnt] = lage
                    cnt += 1

        tri = Delaunay(np.vstack((new_mgfes, new_mets, new_lages)).T)

        # update grid
        self.grid = new_grid

        return tri

    def eval(self, lage, met, mgfe, log=True):
        """
        This is main method of interpolator which return evaluated model
        spectrum for given parameters.
        """
        point = [mgfe, met, lage]

        # take information about calculated triangulation.
        tri = self.coefficients

        current_simplex = tri.find_simplex(point)
        if current_simplex == -1:
            # this is case when parameters outside of grid parameter coverage.
            # Take 4 closest nodes of the grid.
            distance = linalg.norm(tri.points - point, axis=1)
            idx4 = distance.argsort()[:4]
        else:
            # Take 4 nodes of the grid which form simplex covered given point.
            idx4 = tri.simplices[current_simplex]

        # Calculates weights of each chosen node.
        simplex_verices = tri.points[idx4]
        distances = linalg.norm(simplex_verices - point, axis=1)
        weights = 1.0/distances**2 / np.sum( 1.0/distances**2 )

        if log:
            spectrum = 10.0**np.dot(weights, np.log10(self.grid[idx4]))
        else:
            spectrum = np.dot(weights, self.grid[idx4])

        return spectrum


class MilesTriangulationInterpolator(TriangulationInterpolator):
    def __init__(self, gridfile='../data/miles.fits', *args, **kwargs):
        self.gridname = 'MILES'
        self.gridfile = gridfile
        super().__init__(*args, **kwargs)


class MilesSplineInterpolator(SplineInterpolator):
    def __init__(self, gridfile='../data/miles.fits', *args, **kwargs):
        self.gridname = 'MILES'
        self.gridfile = gridfile
        super().__init__(*args, **kwargs)
