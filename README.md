# pysfitter
[![Build Status](https://travis-ci.com/ivan-katkov/pysfitter.svg?token=eyetymndeCTK9Vn7smY1&branch=master)](https://travis-ci.com/ivan-katkov/pysfitter)
[![codecov](https://codecov.io/gh/ivan-katkov/pysfitter/branch/master/graph/badge.svg?token=SboYLyXWGW)](https://codecov.io/gh/ivan-katkov/pysfitter)

This is a Python package for galaxy spectra fitting.

`pysfitter` is an ideological successor of the IDL-based `NBursts` full spectral fitting code (Chilingarian et al. [2007a](https://ui.adsabs.harvard.edu/#abs/2007IAUS..241..175C/abstract), [2007b](https://ui.adsabs.harvard.edu/#abs/2007MNRAS.376.1033C/abstract)).

## Functionality
- [ ] Support of 1d, long-slit and IFU data including LSF information.
- [ ] Few stellar population models (Pegase.HR, MILES)
- [ ] Few kind of SFH: 1SSP, nSSP, SFH, exp-SFH
- [ ] SED + spectra fitting
- [ ] emcee or other Bayesian approach
- [ ] Expand functionality to fit stellar spectra as well.
- [ ] Comprehensive examples with numerous use cases
