Core submodules
===============

pysfitter.spectrum module
-------------------------

.. automodule:: pysfitter.spectrum
    :members:
    :undoc-members:
    :show-inheritance:

pysfitter.losvd module
-----------------------------

.. automodule:: pysfitter.losvd
    :members:
    :undoc-members:
    :show-inheritance:


pysfitter.models module
-----------------------

.. automodule:: pysfitter.models
    :members:
    :undoc-members:
    :show-inheritance:


pysfitter.fitter module
-----------------------

.. automodule:: pysfitter._fitter
    :members:
    :undoc-members:
    :show-inheritance:

pysfitter.lsf module
-----------------------

.. automodule:: pysfitter.lsf
    :members:
    :undoc-members:
    :show-inheritance:


pysfitter.interpolator module
-----------------------------

.. automodule:: pysfitter.interpolator
    :members:
    :undoc-members:
    :show-inheritance:


Utility and settings submodules
===============================

pysfitter.settings module
-------------------------

.. automodule:: pysfitter.settings
    :members:
    :undoc-members:
    :show-inheritance:


pysfitter.utils module
----------------------

.. automodule:: pysfitter.utils
    :members:
    :undoc-members:
    :show-inheritance:
