.. pysfitter documentation master file, created by
   sphinx-quickstart on Wed Aug 15 03:06:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



*pysfitter* documentation
=====================================

This is a Python package for galaxy spectra fitting.

`pysfitter` is an ideological successor of the IDL-based `NBursts` full spectral fitting code (Chilingarian et al. `2007a <https://ui.adsabs.harvard.edu/#abs/2007IAUS..241..175C/abstract>`_, `2007b <https://ui.adsabs.harvard.edu/#abs/2007MNRAS.376.1033C/abstract>`_).


.. toctree::
   :maxdepth: 2

   tutorials

API Reference
==================

.. toctree::
   :maxdepth: 2

   api/pysfitter.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
